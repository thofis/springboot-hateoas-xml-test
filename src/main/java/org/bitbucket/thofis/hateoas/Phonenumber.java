package org.bitbucket.thofis.hateoas;

/**
 * Created by tom on 14.01.17.
 */
public class Phonenumber {

    private String number;

    Phonenumber() {

    }

    public Phonenumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
