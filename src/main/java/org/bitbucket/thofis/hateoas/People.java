package org.bitbucket.thofis.hateoas;

import org.springframework.hateoas.ResourceSupport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tom on 14.01.17.
 */
@XmlRootElement
public class People extends ResourceSupport {

    private List<Person> people = new ArrayList<>();

    People() {

    }

    public People(Person... people) {
        this.people = Arrays.asList(people);
    }

    @XmlElement(name = "person")
    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }
}


