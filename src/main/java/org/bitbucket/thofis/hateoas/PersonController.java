package org.bitbucket.thofis.hateoas;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by tom on 14.01.17.
 */
@RestController
public class PersonController {

    @RequestMapping(path = "/person/{lastname}", method = RequestMethod.GET, produces = "application/xml")
    public Person getPerson(@PathVariable(name = "lastname") final String lastname) {
        Person person = new Person("Thomas", lastname);

        Link self = linkTo(methodOn(PersonController.class).getPerson(lastname)).withSelfRel();

        person.add(self);
        person.getPhonenumbers().add(new Phonenumber("2345"));

        //Resource<Person> personResource = new Resource<>(person);
        //personResource.add(self);
        return person;
    }

    @RequestMapping(path = "/people", method = RequestMethod.GET, produces = {"application/xml", "application/json"})
    public Resources<Person> people() {
        Person person1 = new Person("Thomas", "Fischer");
        person1.add(linkTo(methodOn(PersonController.class).getPerson("Fischer")).withSelfRel());
        Person person2 = new Person("John", "Doe");
        person2.add(linkTo(methodOn(PersonController.class).getPerson("Doe")).withSelfRel());
        Resources resources = new Resources(Arrays.asList(person1,person2));
        resources.add(linkTo(methodOn(PersonController.class).people()).withSelfRel());
        return resources;
    }

    @RequestMapping(path = "/people2", method = RequestMethod.GET, produces = {"application/xml", "application/json"})
    public People people2() {
        Person person1 = new Person("Thomas", "Fischer");
        person1.add(linkTo(methodOn(PersonController.class).getPerson("Fischer")).withSelfRel());
        Person person2 = new Person("John", "Doe");
        person2.add(linkTo(methodOn(PersonController.class).getPerson("Doe")).withSelfRel());
        People people = new People(person1, person2);
        people.add(linkTo(methodOn(PersonController.class).people2()).withSelfRel());
        return people;
    }
}

