package org.bitbucket.thofis.hateoas;

import org.springframework.hateoas.ResourceSupport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@XmlRootElement
public class Person extends ResourceSupport {

    private String firstName;
    private String lastName;

    private Set<Phonenumber> phonenumbers = new HashSet<>();

    public Person() {

    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @XmlElement
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @XmlElement
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Phonenumber> getPhonenumbers() {
        return phonenumbers;
    }

    public void setPhonenumbers(Set<Phonenumber> phonenumbers) {
        this.phonenumbers = phonenumbers;
    }
}
