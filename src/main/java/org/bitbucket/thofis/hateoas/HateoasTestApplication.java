package org.bitbucket.thofis.hateoas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Iterator;
import java.util.List;

@SpringBootApplication
@EnableSwagger2
public class HateoasTestApplication {

//	@Bean
//	public MarshallingHttpMessageConverter marshallingMessageConverter() {
//		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//		marshaller.setClassesToBeBound(PagedResources.class, Resource.class);
//
//		MarshallingHttpMessageConverter converter = new MarshallingHttpMessageConverter();
//		converter.setMarshaller(marshaller);
//		converter.setUnmarshaller(marshaller);
//		return converter;
//	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean
	Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setClassesToBeBound(Person.class);
		return marshaller;
	}

	@Bean
	HttpMessageConverters httpMessageConverters() {
		HttpMessageConverters converters = new HttpMessageConverters() {
			@Override
			protected List<HttpMessageConverter<?>> postProcessConverters(
					List<HttpMessageConverter<?>> converters) {
				for (Iterator<HttpMessageConverter<?>> iterator = converters.iterator(); iterator
						.hasNext(); ) {
					HttpMessageConverter<?> next = iterator.next();
					System.out.println("MessageConverter: "+next.toString());
//					if (next instanceof Jaxb2RootElementHttpMessageConverter) {
//						iterator.remove();
//					}

				}
				return converters;
			}
		};
		return converters;
	}

	public static void main(String[] args) {
		SpringApplication.run(HateoasTestApplication.class, args);
	}
}
